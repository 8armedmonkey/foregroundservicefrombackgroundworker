package com.eightam.foregroundservice

import android.app.Application
import android.util.Log
import androidx.core.app.NotificationManagerCompat
import androidx.work.Configuration
import androidx.work.WorkManager
import com.eightam.foregroundservice.utils.Singletons
import com.eightam.foregroundservice.utils.notification.NotificationChannels
import com.eightam.foregroundservice.utils.worker.AppWorkerFactory

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        init()
    }

    private fun init() {
        initWorkManager()
        initNotificationChannel()
        initSingletons()
    }

    private fun initWorkManager() {
        WorkManager.initialize(
            this,
            Configuration.Builder()
                .setMinimumLoggingLevel(if (BuildConfig.DEBUG) Log.DEBUG else Log.INFO)
                .setWorkerFactory(AppWorkerFactory())
                .build()
        )
    }

    private fun initNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManagerCompat.from(this)
                .createNotificationChannel(NotificationChannels.getDefault(this))
        }
    }

    private fun initSingletons() {
        Singletons.init(this)
    }

}