package com.eightam.foregroundservice.main

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.eightam.foregroundservice.R
import com.eightam.foregroundservice.timer.*
import com.eightam.foregroundservice.utils.Singletons
import com.eightam.foregroundservice.utils.presentation.adapter.TimerAdapter
import java.util.*

class MainActivity : AppCompatActivity() {

    private val adapter: TimerAdapter by lazy {
        TimerAdapter(object : TimerAdapter.Listener {
            override fun onRemoveTimerPerformed(id: UUID) {
                viewModel.onRemoveTimerPerformed(id)
            }
        })
    }

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(
            this,
            MainViewModel.Factory(
                RetrieveAllTimersLiveDataUseCase(Singletons.getTimerRepository()),
                AddCountUpTimerUseCase(Singletons.getTimerRepository()),
                AddCountDownTimerUseCase(Singletons.getTimerRepository()),
                RemoveTimerUseCase(Singletons.getTimerRepository()),
                ClearTimerUseCase(Singletons.getTimerRepository()),
                ScheduleTimerNotificationsUseCase(Singletons.getTimerNotificationSchedulingService())
            )
        ).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpViews()
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        viewModel.start()
    }

    override fun onStop() {
        viewModel.stop()
        super.onStop()
    }

    private fun setUpViews() {
        findViewById<RecyclerView>(R.id.timer_recycler_view).adapter = adapter

        findViewById<Button>(R.id.add_timer).setOnClickListener {
            viewModel.onAddTimerPerformed()
        }

        findViewById<Button>(R.id.clear_timers).setOnClickListener {
            viewModel.onClearTimersPerformed()
        }
    }

    private fun observeViewModel() {
        viewModel.getTimersLiveData().observe(this, { timerPresentations ->
            timerPresentations?.let { adapter.submitList(it) }
        })

        viewModel.getMainEventLiveData().observe(this, { event ->
            when (event?.data) {
                is MainEvent.ShowAddTimerDialog -> showAddTimerDialog()
            }
        })
    }

    private fun showAddTimerDialog() {
        viewModel.onAddCountUpTimerPerformed(Date())
    }

}