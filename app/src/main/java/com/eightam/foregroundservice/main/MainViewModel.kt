package com.eightam.foregroundservice.main

import androidx.lifecycle.*
import com.eightam.foregroundservice.timer.*
import com.eightam.foregroundservice.utils.event.Event
import com.eightam.foregroundservice.utils.presentation.data.TimerPresentation
import com.eightam.foregroundservice.utils.presentation.data.toTimerPresentation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class MainViewModel(
    private val retrieveAllTimersLiveDataUseCase: RetrieveAllTimersLiveDataUseCase,
    private val addCountUpTimerUseCase: AddCountUpTimerUseCase,
    private val addCountDownTimerUseCase: AddCountDownTimerUseCase,
    private val removeTimerUseCase: RemoveTimerUseCase,
    private val clearTimerUseCase: ClearTimerUseCase,
    private val scheduleTimerNotificationsUseCase: ScheduleTimerNotificationsUseCase
) : ViewModel() {

    private val mainEventLiveData = MutableLiveData<Event<MainEvent>>()

    fun start() {
        // No-op.
    }

    fun stop() {
        scheduleTimerNotificationsUseCase.execute()
    }

    fun onAddTimerPerformed() {
        mainEventLiveData.value = Event(data = MainEvent.ShowAddTimerDialog)
    }

    fun onAddCountUpTimerPerformed(startAt: Date) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                addCountUpTimerUseCase.execute(startAt)
            }
        }
    }

    fun onAddCountDownTimerPerformed(stopAt: Date) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                addCountDownTimerUseCase.execute(stopAt)
            }
        }
    }

    fun onRemoveTimerPerformed(id: UUID) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                removeTimerUseCase.execute(id)
            }
        }
    }

    fun onClearTimersPerformed() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                clearTimerUseCase.execute()
            }
        }
    }

    fun getTimersLiveData(): LiveData<List<TimerPresentation>> {
        return Transformations.map(retrieveAllTimersLiveDataUseCase.execute()) { timers ->
            timers.map { it.toTimerPresentation() }
        }
    }

    fun getMainEventLiveData(): LiveData<Event<MainEvent>> {
        return mainEventLiveData
    }

    class Factory(
        private val retrieveAllTimersLiveDataUseCase: RetrieveAllTimersLiveDataUseCase,
        private val addCountUpTimerUseCase: AddCountUpTimerUseCase,
        private val addCountDownTimerUseCase: AddCountDownTimerUseCase,
        private val removeTimerUseCase: RemoveTimerUseCase,
        private val clearTimerUseCase: ClearTimerUseCase,
        private val scheduleTimerNotificationsUseCase: ScheduleTimerNotificationsUseCase
    ) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return modelClass.cast(
                MainViewModel(
                    retrieveAllTimersLiveDataUseCase,
                    addCountUpTimerUseCase,
                    addCountDownTimerUseCase,
                    removeTimerUseCase,
                    clearTimerUseCase,
                    scheduleTimerNotificationsUseCase
                )
            ) ?: throw RuntimeException("Unable to create ViewModel: $modelClass")
        }

    }

}