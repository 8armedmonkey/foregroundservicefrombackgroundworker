package com.eightam.foregroundservice.main

sealed class MainEvent {

    object ShowAddTimerDialog : MainEvent()

}