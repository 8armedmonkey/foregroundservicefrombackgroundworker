package com.eightam.foregroundservice.utils.presentation.data

import java.util.*
import java.util.concurrent.TimeUnit

sealed class TimerPresentation {

    data class CountUpTimerPresentation(
        val id: UUID,
        val startAt: Date,
        val durationInSeconds: Long
    ) : TimerPresentation()

    data class CountDownTimerPresentation(
        val id: UUID,
        val stopAt: Date,
        val durationInSeconds: Long
    ) : TimerPresentation()

    companion object {

        fun formatDuration(durationInSeconds: Long): String {
            return String.format(
                "%02d:%02d:%02d",
                TimeUnit.SECONDS.toHours(durationInSeconds) % 60,
                TimeUnit.SECONDS.toMinutes(durationInSeconds) % 60,
                TimeUnit.SECONDS.toSeconds(durationInSeconds) % 60
            )
        }

    }

}