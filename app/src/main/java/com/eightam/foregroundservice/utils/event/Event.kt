package com.eightam.foregroundservice.utils.event

import java.util.*

data class Event<T>(
    val id: UUID = UUID.randomUUID(),
    val data: T
)