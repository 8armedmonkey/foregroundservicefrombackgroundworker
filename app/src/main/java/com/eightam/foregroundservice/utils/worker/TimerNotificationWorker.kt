package com.eightam.foregroundservice.utils.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.eightam.foregroundservice.timer.ShowTimerNotificationsUseCase

class TimerNotificationWorker(
    private val showTimerNotificationsUseCase: ShowTimerNotificationsUseCase,
    context: Context,
    workerParams: WorkerParameters
) : Worker(context, workerParams) {

    override fun doWork(): Result {
        showTimerNotificationsUseCase.execute()
        return Result.success()
    }

}