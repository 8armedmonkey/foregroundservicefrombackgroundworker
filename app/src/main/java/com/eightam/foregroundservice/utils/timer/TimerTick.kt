package com.eightam.foregroundservice.utils.timer

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.ticker

@ObsoleteCoroutinesApi
class TimerTick {

    private val coroutineScope: CoroutineScope = CoroutineScope(Job() + Dispatchers.Main)
    private var tickerChannel: ReceiveChannel<Unit>? = null

    fun start(tickIntervalMillis: Long, onTick: () -> Unit) {
        synchronized(this) {
            stop()

            tickerChannel = ticker(
                delayMillis = tickIntervalMillis,
                initialDelayMillis = 0
            ).also { tickerChannel ->
                coroutineScope.launch {
                    for (tick in tickerChannel) {
                        onTick()
                    }
                }
            }
        }
    }

    fun stop() {
        synchronized(this) {
            tickerChannel?.cancel()
            tickerChannel = null
        }
    }

    fun destroy() {
        synchronized(this) {
            coroutineScope.cancel()
        }
    }

}