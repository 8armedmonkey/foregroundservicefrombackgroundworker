package com.eightam.foregroundservice.utils.timer

import android.content.Context
import androidx.work.Constraints
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.eightam.foregroundservice.timer.TimerNotificationSchedulingService
import com.eightam.foregroundservice.utils.worker.TimerNotificationWorker
import java.util.concurrent.TimeUnit

class TimerNotificationSchedulingServiceImpl(
    private val context: Context
) : TimerNotificationSchedulingService {

    override fun schedule(initialDelayMillis: Long) {
        WorkManager.getInstance(context).enqueue(
            OneTimeWorkRequest.Builder(TimerNotificationWorker::class.java)
                .setInitialDelay(initialDelayMillis, TimeUnit.MILLISECONDS)
                .setConstraints(Constraints.NONE)
                .build()
        )
    }

}