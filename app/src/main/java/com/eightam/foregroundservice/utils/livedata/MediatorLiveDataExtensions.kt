package com.eightam.foregroundservice.utils.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

fun <R> MediatorLiveData<R>.zip(
    vararg sources: LiveData<*>,
    onChanged: (Array<*>) -> Unit
) {
    val values = arrayOfNulls<Any>(sources.size)

    sources.forEachIndexed { index, liveData ->
        addSource(liveData) { value ->
            values[index] = value
            onChanged(values)
        }
    }
}