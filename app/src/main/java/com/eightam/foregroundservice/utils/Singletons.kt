package com.eightam.foregroundservice.utils

import android.content.Context
import androidx.room.Room
import com.eightam.foregroundservice.timer.TimerNotificationSchedulingService
import com.eightam.foregroundservice.timer.TimerNotificationService
import com.eightam.foregroundservice.timer.TimerRepository
import com.eightam.foregroundservice.utils.room.AppDatabase
import com.eightam.foregroundservice.utils.room.TimerRepositoryImpl
import com.eightam.foregroundservice.utils.timer.TimerNotificationSchedulingServiceImpl
import com.eightam.foregroundservice.utils.timer.TimerNotificationServiceImpl

object Singletons {

    private lateinit var appDatabase: AppDatabase
    private lateinit var timerRepository: TimerRepository
    private lateinit var timerNotificationSchedulingService: TimerNotificationSchedulingService
    private lateinit var timerNotificationService: TimerNotificationService

    fun init(appContext: Context) {
        appDatabase = Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            AppDatabase.DATABASE_NAME
        ).build()

        timerRepository = TimerRepositoryImpl(appDatabase)
        timerNotificationSchedulingService = TimerNotificationSchedulingServiceImpl(appContext)
        timerNotificationService = TimerNotificationServiceImpl(appContext)
    }

    fun getTimerRepository(): TimerRepository {
        return timerRepository
    }

    fun getTimerNotificationSchedulingService(): TimerNotificationSchedulingService {
        return timerNotificationSchedulingService
    }

    fun getTimerNotificationService(): TimerNotificationService {
        return timerNotificationService
    }

}