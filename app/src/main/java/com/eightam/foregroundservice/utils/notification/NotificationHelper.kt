package com.eightam.foregroundservice.utils.notification

import android.app.Notification
import android.app.NotificationManager
import android.content.Context

class NotificationHelper(private val context: Context) {

    private val notificationManager: NotificationManager
        get() = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    fun notify(tag: String? = null, id: Int, notification: Notification) {
        notificationManager.notify(tag, id, notification)
    }

    fun cancel(tag: String? = null, id: Int) {
        notificationManager.cancel(tag, id)
    }

}