package com.eightam.foregroundservice.utils.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.eightam.foregroundservice.R
import com.eightam.foregroundservice.utils.presentation.data.TimerPresentation
import com.eightam.foregroundservice.utils.presentation.data.TimerPresentation.CountDownTimerPresentation
import com.eightam.foregroundservice.utils.presentation.data.TimerPresentation.CountUpTimerPresentation
import java.util.*

class TimerAdapter(
    private val listener: Listener
) : ListAdapter<TimerPresentation, TimerAdapter.ViewHolder>(ItemCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_timer, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }

    interface Listener {

        fun onRemoveTimerPerformed(id: UUID)

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val infoTextView: TextView = itemView.findViewById(R.id.info_text_view)
        private val durationTextView: TextView = itemView.findViewById(R.id.duration_text_view)
        private val removeTimerButton: Button = itemView.findViewById(R.id.remove_timer_button)

        fun bind(timerPresentation: TimerPresentation, listener: Listener) {
            when (timerPresentation) {
                is CountUpTimerPresentation -> {
                    infoTextView.text = timerPresentation.id.toString()
                    durationTextView.text = TimerPresentation.formatDuration(
                        timerPresentation.durationInSeconds
                    )
                    removeTimerButton.setOnClickListener {
                        listener.onRemoveTimerPerformed(timerPresentation.id)
                    }
                }
                is CountDownTimerPresentation -> {
                    infoTextView.text = timerPresentation.id.toString()
                    durationTextView.text = TimerPresentation.formatDuration(
                        timerPresentation.durationInSeconds
                    )
                    removeTimerButton.setOnClickListener {
                        listener.onRemoveTimerPerformed(timerPresentation.id)
                    }
                }
            }
        }

    }

    object ItemCallback : DiffUtil.ItemCallback<TimerPresentation>() {

        override fun areItemsTheSame(
            oldItem: TimerPresentation,
            newItem: TimerPresentation
        ): Boolean {
            return when {
                oldItem is CountUpTimerPresentation
                        && newItem is CountUpTimerPresentation -> oldItem.id == newItem.id

                oldItem is CountDownTimerPresentation
                        && newItem is CountDownTimerPresentation -> oldItem.id == newItem.id

                else -> false
            }
        }

        override fun areContentsTheSame(
            oldItem: TimerPresentation,
            newItem: TimerPresentation
        ): Boolean {
            return oldItem == newItem
        }
    }

}