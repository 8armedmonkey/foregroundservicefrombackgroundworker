package com.eightam.foregroundservice.utils.notification

import android.app.Notification

class ForegroundNotifications(
    private val foregroundServiceHelper: ForegroundServiceHelper,
    private val notificationHelper: NotificationHelper
) {

    var activeNotificationId: Int? = null
        private set

    val notificationIds: Set<Int>
        get() = entries.keys

    val isEmpty: Boolean
        get() = entries.isEmpty()

    private val entries: LinkedHashMap<Int, Notification> = LinkedHashMap()

    fun notify(notificationId: Int, notification: Notification) {
        synchronized(this) {
            // If the notification already exists then update it. Otherwise create a new one.
            if (contains(notificationId)) {
                notificationHelper.notify(
                    id = notificationId,
                    notification = notification
                )
                entries[notificationId] = notification
            } else {
                notifyAndDetachPrevious(
                    notificationId = notificationId,
                    notification = notification
                )
            }
        }
    }

    fun notifyAndDetachPrevious(notificationId: Int, notification: Notification) {
        synchronized(this) {
            foregroundServiceHelper.startForegroundAndDetachPreviousNotification(
                notificationId,
                notification
            )
            entries[notificationId] = notification
            activeNotificationId = notificationId
        }
    }

    fun notifyAndRemovePrevious(notificationId: Int, notification: Notification) {
        synchronized(this) {
            foregroundServiceHelper.startForegroundAndRemovePreviousNotification(
                notificationId,
                notification
            )
            entries[notificationId] = notification
            activeNotificationId = notificationId
        }
    }

    fun cancel(notificationId: Int) {
        synchronized(this) {
            if (notificationId == activeNotificationId) {
                val newActiveNotificationId = entries.keys.findLast { it != activeNotificationId }
                val notification = entries[newActiveNotificationId]

                if (newActiveNotificationId != null && notification != null) {
                    notifyAndDetachPrevious(newActiveNotificationId, notification)
                }
            }

            entries.remove(notificationId)

            if (isEmpty) {
                foregroundServiceHelper.stopForeground()
            } else {
                notificationHelper.cancel(id = notificationId)
            }
        }
    }

    fun contains(notificationId: Int): Boolean =
        synchronized(this) {
            entries.containsKey(notificationId)
        }

}