package com.eightam.foregroundservice.utils.timer

import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.ServiceCompat
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.eightam.foregroundservice.R
import com.eightam.foregroundservice.timer.RetrieveAllTimersLiveDataUseCase
import com.eightam.foregroundservice.timer.Timer
import com.eightam.foregroundservice.utils.Singletons
import com.eightam.foregroundservice.utils.livedata.zip
import com.eightam.foregroundservice.utils.notification.ForegroundNotifications
import com.eightam.foregroundservice.utils.notification.ForegroundServiceHelper
import com.eightam.foregroundservice.utils.notification.NotificationChannels
import com.eightam.foregroundservice.utils.notification.NotificationHelper
import com.eightam.foregroundservice.utils.presentation.data.TimerPresentation.Companion.formatDuration
import com.eightam.foregroundservice.utils.type.toLong
import kotlinx.coroutines.ObsoleteCoroutinesApi
import java.util.concurrent.TimeUnit

class TimerNotificationsPlatformService : LifecycleService() {

    private val foregroundNotifications: ForegroundNotifications by lazy {
        ForegroundNotifications(
            ForegroundServiceHelper(this),
            NotificationHelper(applicationContext)
        )
    }

    @ObsoleteCoroutinesApi
    private val timerTick = TimerTick()

    private val timerTickLiveData = MutableLiveData<Unit>()
    private val activeTimersLiveData = MediatorLiveData<List<Timer>>()

    @ObsoleteCoroutinesApi
    override fun onCreate() {
        super.onCreate()
        observeTimersLiveData()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    @ObsoleteCoroutinesApi
    override fun onDestroy() {
        destroyTimer()
        super.onDestroy()
    }

    @ObsoleteCoroutinesApi
    private fun observeTimersLiveData() {
        val allTimersLiveData =
            RetrieveAllTimersLiveDataUseCase(Singletons.getTimerRepository()).execute()

        activeTimersLiveData.apply {
            zip(allTimersLiveData, timerTickLiveData) { values ->
                val currentTimeMillis = System.currentTimeMillis()
                val timers = (values[0] as? List<*>)
                    ?.mapNotNull { it as? Timer }
                    ?.filter { it.getCounterDurationMillis(currentTimeMillis) > 0L }

                if (timers != null) {
                    postValue(timers)

                    if (timers.isEmpty()) {
                        stopTimer()
                    }
                }
            }
        }

        allTimersLiveData.observe(this, { timers ->
            if (timers.isNotEmpty()) {
                startTimer()
            } else {
                stopTimer()
            }
        })

        activeTimersLiveData.observe(this, { timers ->
            val notificationIds = foregroundNotifications.notificationIds
            val timerNotificationIds = timers.map { it.id.toLong().toInt() }
            val inactiveNotificationIds = notificationIds.filterNot {
                timerNotificationIds.contains(it)
            }

            if (timers.isNotEmpty()) {
                timers.forEach { showNotification(it) }
                inactiveNotificationIds.forEach { cancelNotification(it) }
            } else {
                inactiveNotificationIds.forEach { cancelNotification(it) }
                startForegroundAndStopImmediately()
            }
        })
    }

    private fun startForegroundAndStopImmediately() {
        val notification =
            NotificationCompat.Builder(applicationContext, NotificationChannels.DEFAULT_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setOnlyAlertOnce(true)
                .setNotificationSilent()
                .build()

        foregroundNotifications.notifyAndRemovePrevious(DUMMY_NOTIFICATION_ID, notification)
        ServiceCompat.stopForeground(this, ServiceCompat.STOP_FOREGROUND_REMOVE)
        stopSelf()
    }

    private fun showNotification(timer: Timer) {
        val timerNotificationId = timer.id.toLong().toInt()
        val durationInSeconds = TimeUnit.MILLISECONDS.toSeconds(
            timer.getCounterDurationMillis(System.currentTimeMillis())
        )
        val notification =
            NotificationCompat.Builder(applicationContext, NotificationChannels.DEFAULT_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(getString(R.string.timer_notification_title, timer.id.toString()))
                .setContentText(formatDuration(durationInSeconds))
                .setAutoCancel(false)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setNotificationSilent()
                .build()

        foregroundNotifications.notify(timerNotificationId, notification)
    }

    private fun cancelNotification(notificationId: Int) {
        foregroundNotifications.cancel(notificationId)
    }

    @ObsoleteCoroutinesApi
    private fun startTimer() {
        timerTick.start(TIMER_TICK_INTERVAL_MILLIS) {
            timerTickLiveData.postValue(Unit)
        }
    }

    @ObsoleteCoroutinesApi
    private fun stopTimer() {
        timerTick.stop()
    }

    @ObsoleteCoroutinesApi
    private fun destroyTimer() {
        timerTick.destroy()
    }

    companion object {

        private const val TIMER_TICK_INTERVAL_MILLIS = 1000L
        private const val DUMMY_NOTIFICATION_ID = Integer.MAX_VALUE

        fun getStartIntent(context: Context): Intent {
            return Intent(context, TimerNotificationsPlatformService::class.java)
        }

    }

}