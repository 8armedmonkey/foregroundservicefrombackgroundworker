package com.eightam.foregroundservice.utils.notification

import android.app.Notification
import android.app.Service
import androidx.core.app.ServiceCompat

class ForegroundServiceHelper(private val service: Service) {

    fun startForegroundAndDetachPreviousNotification(
        notificationId: Int,
        notification: Notification
    ) {
        ServiceCompat.stopForeground(service, ServiceCompat.STOP_FOREGROUND_DETACH)
        service.startForeground(notificationId, notification)
    }

    fun startForegroundAndRemovePreviousNotification(
        notificationId: Int,
        notification: Notification
    ) {
        ServiceCompat.stopForeground(service, ServiceCompat.STOP_FOREGROUND_REMOVE)
        service.startForeground(notificationId, notification)
    }

    fun stopForeground() {
        ServiceCompat.stopForeground(service, ServiceCompat.STOP_FOREGROUND_REMOVE)
    }

    fun stopSelf() {
        service.stopSelf()
    }

}