package com.eightam.foregroundservice.utils.room

import com.eightam.foregroundservice.timer.Timer
import java.util.*

fun Timer.toTimerEntity(): TimerEntity {
    return when (this) {
        is Timer.CountUpTimer -> TimerEntity(
            id = id.toString(),
            startAt = startAt.time,
            timerType = TimerEntity.TIMER_TYPE_COUNT_UP
        )
        is Timer.CountDownTimer -> TimerEntity(
            id = id.toString(),
            startAt = stopAt.time,
            timerType = TimerEntity.TIMER_TYPE_COUNT_DOWN
        )
    }
}

fun TimerEntity.toTimer(): Timer {
    return when (timerType) {
        TimerEntity.TIMER_TYPE_COUNT_UP -> Timer.CountUpTimer(
            id = UUID.fromString(id),
            startAt = startAt?.let { Date(startAt) } ?: throw IllegalArgumentException()
        )
        TimerEntity.TIMER_TYPE_COUNT_DOWN -> Timer.CountDownTimer(
            id = UUID.fromString(id),
            stopAt = stopAt?.let { Date(stopAt) } ?: throw IllegalArgumentException()
        )
        else -> throw IllegalArgumentException()
    }
}