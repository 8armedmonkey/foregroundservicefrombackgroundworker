package com.eightam.foregroundservice.utils.room

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.eightam.foregroundservice.timer.Timer
import com.eightam.foregroundservice.timer.TimerRepository
import java.util.*

class TimerRepositoryImpl(private val db: AppDatabase) : TimerRepository {

    override fun getTimersLiveData(): LiveData<List<Timer>> {
        return Transformations.map(db.timerDao().retrieveAllLiveData()) { timerEntities ->
            timerEntities.map { it.toTimer() }
        }
    }

    override fun store(timer: Timer) {
        db.timerDao().insert(timer.toTimerEntity())
    }

    override fun retrieve(id: UUID): Timer {
        return db.timerDao().retrieve(id.toString()).toTimer()
    }

    override fun remove(id: UUID) {
        db.timerDao().delete(id.toString())
    }

    override fun clear() {
        db.timerDao().deleteAll()
    }

    override fun size(): Int {
        return db.timerDao().count()
    }

}