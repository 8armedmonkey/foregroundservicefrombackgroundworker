package com.eightam.foregroundservice.utils.type

import java.math.BigInteger
import java.nio.ByteBuffer
import java.util.*
import kotlin.math.abs

// Adapted from: http://www.gregbugaj.com/?p=587
fun UUID.toLong(): Long {
    val buffer = ByteBuffer.wrap(ByteArray(16)).apply {
        putLong(leastSignificantBits)
        putLong(mostSignificantBits)
    }
    return abs(BigInteger(buffer.array()).toLong())
}