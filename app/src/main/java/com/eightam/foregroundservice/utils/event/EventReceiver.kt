package com.eightam.foregroundservice.utils.event

import java.util.*

interface EventReceiver<T> {

    fun receive(event: Event<T>)

    fun hasNewEvents(): Boolean

    fun getNewEvents(): List<Event<T>>

    fun markEventsAsOld(eventIds: List<UUID>)

    fun getOldEvents(): List<Event<T>>

    fun discardEvents(eventIds: List<UUID>)

}