package com.eightam.foregroundservice.utils.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TimerDao {

    @Query("SELECT * FROM timer")
    fun retrieveAllLiveData(): LiveData<List<TimerEntity>>

    @Query("SELECT * FROM timer WHERE id = :id")
    fun retrieve(id: String): TimerEntity

    @Insert
    fun insert(timerEntity: TimerEntity)

    @Query("DELETE FROM timer WHERE id = :id")
    fun delete(id: String)

    @Query("DELETE FROM timer")
    fun deleteAll()

    @Query("SELECT COUNT(*) FROM timer")
    fun count(): Int

}