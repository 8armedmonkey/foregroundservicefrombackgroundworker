package com.eightam.foregroundservice.utils.timer

import android.content.Context
import com.eightam.foregroundservice.timer.TimerNotificationService

class TimerNotificationServiceImpl(
    private val context: Context
) : TimerNotificationService {

    override fun showTimerNotifications() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            context.startForegroundService(TimerNotificationsPlatformService.getStartIntent(context))
        } else {
            context.startService(TimerNotificationsPlatformService.getStartIntent(context))
        }
    }

}