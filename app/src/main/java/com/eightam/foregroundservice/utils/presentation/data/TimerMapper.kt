package com.eightam.foregroundservice.utils.presentation.data

import com.eightam.foregroundservice.timer.Timer

fun Timer.toTimerPresentation(): TimerPresentation {
    return when (this) {
        is Timer.CountUpTimer -> TimerPresentation.CountUpTimerPresentation(
            id = id,
            startAt = startAt,
            durationInSeconds = 0
        )
        is Timer.CountDownTimer -> TimerPresentation.CountDownTimerPresentation(
            id = id,
            stopAt = stopAt,
            durationInSeconds = 0
        )
    }
}