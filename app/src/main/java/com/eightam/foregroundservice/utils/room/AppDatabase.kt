package com.eightam.foregroundservice.utils.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [TimerEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun timerDao(): TimerDao

    companion object {

        const val DATABASE_NAME = "app_database"

    }

}