package com.eightam.foregroundservice.utils.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import com.eightam.foregroundservice.R

object NotificationChannels {

    const val DEFAULT_CHANNEL_ID = "default"

    @RequiresApi(Build.VERSION_CODES.O)
    fun getDefault(context: Context): NotificationChannel {
        return NotificationChannel(
            DEFAULT_CHANNEL_ID,
            context.getString(R.string.notification_channel_default),
            NotificationManager.IMPORTANCE_DEFAULT
        ).apply {
            enableVibration(true)
            setShowBadge(false)
        }
    }

}