package com.eightam.foregroundservice.utils.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "timer")
data class TimerEntity(
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "start_at") val startAt: Long? = null,
    @ColumnInfo(name = "stop_at") val stopAt: Long? = null,
    @ColumnInfo(name = "timer_type") val timerType: String
) {

    companion object {

        const val TIMER_TYPE_COUNT_UP = "COUNT_UP"
        const val TIMER_TYPE_COUNT_DOWN = "COUNT_DOWN"

    }

}