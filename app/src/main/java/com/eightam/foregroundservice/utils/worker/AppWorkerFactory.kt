package com.eightam.foregroundservice.utils.worker

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import com.eightam.foregroundservice.timer.ShowTimerNotificationsUseCase
import com.eightam.foregroundservice.utils.Singletons

class AppWorkerFactory : WorkerFactory() {

    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ): ListenableWorker? {
        return when (workerClassName) {
            TimerNotificationWorker::class.java.name -> TimerNotificationWorker(
                ShowTimerNotificationsUseCase(
                    Singletons.getTimerRepository(),
                    Singletons.getTimerNotificationService()
                ),
                appContext,
                workerParameters
            )
            else -> null
        }
    }

}