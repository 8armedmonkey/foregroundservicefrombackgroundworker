package com.eightam.foregroundservice.utils.event

import java.util.*
import kotlin.collections.LinkedHashMap

class SimpleEventReceiver<T> private constructor(
    oldEventIds: Set<UUID>,
    events: List<Event<T>>
) : EventReceiver<T> {

    private val oldEventIds = mutableSetOf<UUID>()
    private val events = LinkedHashMap<UUID, Event<T>>()

    init {
        events.forEach {
            this.events[it.id] = it

            if (oldEventIds.contains(it.id)) {
                this.oldEventIds.add(it.id)
            }
        }
    }

    override fun receive(event: Event<T>) {
        synchronized(this) {
            if (!oldEventIds.contains(event.id)) {
                events[event.id] = event
            }
        }
    }

    override fun hasNewEvents(): Boolean {
        synchronized(this) {
            return events.keys.any { !oldEventIds.contains(it) }
        }
    }

    override fun getNewEvents(): List<Event<T>> {
        synchronized(this) {
            return events.values.filter { !oldEventIds.contains(it.id) }
        }
    }

    override fun markEventsAsOld(eventIds: List<UUID>) {
        synchronized(this) {
            oldEventIds.addAll(eventIds.filter { events.keys.contains(it) })
        }
    }

    override fun getOldEvents(): List<Event<T>> {
        synchronized(this) {
            return events.values.filter { oldEventIds.contains(it.id) }
        }
    }

    override fun discardEvents(eventIds: List<UUID>) {
        synchronized(this) {
            eventIds.forEach {
                oldEventIds.remove(it)
                events.remove(it)
            }
        }
    }

    class Builder<T> {

        private var oldEventIds = setOf<UUID>()
        private var events = listOf<Event<T>>()

        fun oldEventIds(oldEventIds: Set<UUID>): Builder<T> {
            this.oldEventIds = oldEventIds
            return this
        }

        fun events(events: List<Event<T>>): Builder<T> {
            this.events = events
            return this
        }

        fun build(): SimpleEventReceiver<T> {
            return SimpleEventReceiver(
                oldEventIds = oldEventIds,
                events = events
            )
        }

    }

}