package com.eightam.foregroundservice.timer

import java.util.*
import kotlin.math.max

sealed class Timer(val id: UUID = UUID.randomUUID()) {

    abstract fun getCounterDurationMillis(currentTimeMillis: Long): Long

    class CountUpTimer(
        id: UUID = UUID.randomUUID(),
        val startAt: Date
    ) : Timer(id) {

        override fun getCounterDurationMillis(currentTimeMillis: Long): Long {
            return max(0, currentTimeMillis - startAt.time)
        }

    }

    class CountDownTimer(
        id: UUID = UUID.randomUUID(),
        val stopAt: Date
    ) : Timer(id) {

        override fun getCounterDurationMillis(currentTimeMillis: Long): Long {
            return max(0, stopAt.time - currentTimeMillis)
        }

    }

}