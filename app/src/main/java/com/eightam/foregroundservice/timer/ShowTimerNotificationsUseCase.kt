package com.eightam.foregroundservice.timer

class ShowTimerNotificationsUseCase(
    private val timerRepository: TimerRepository,
    private val timerNotificationsService: TimerNotificationService
) {

    fun execute() {
        if (timerRepository.size() > 0) {
            timerNotificationsService.showTimerNotifications()
        }
    }

}