package com.eightam.foregroundservice.timer

class ScheduleTimerNotificationsUseCase(
    private val timerNotificationSchedulingService: TimerNotificationSchedulingService
) {

    fun execute() {
        timerNotificationSchedulingService.schedule(INITIAL_DELAY_MILLIS)
    }

    companion object {

        const val INITIAL_DELAY_MILLIS = 10000L

    }

}