package com.eightam.foregroundservice.timer

interface TimerNotificationService {

    fun showTimerNotifications()

}