package com.eightam.foregroundservice.timer

import java.util.*

class AddCountUpTimerUseCase(private val timerRepository: TimerRepository) {

    fun execute(startAt: Date) {
        timerRepository.store(Timer.CountUpTimer(startAt = startAt))
    }

}