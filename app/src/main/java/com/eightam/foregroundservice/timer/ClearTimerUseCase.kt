package com.eightam.foregroundservice.timer

class ClearTimerUseCase(private val timerRepository: TimerRepository) {

    fun execute() {
        timerRepository.clear()
    }

}