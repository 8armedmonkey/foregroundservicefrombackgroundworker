package com.eightam.foregroundservice.timer

import java.util.*

class AddCountDownTimerUseCase(private val timerRepository: TimerRepository) {

    fun execute(stopAt: Date) {
        timerRepository.store(Timer.CountDownTimer(stopAt = stopAt))
    }

}