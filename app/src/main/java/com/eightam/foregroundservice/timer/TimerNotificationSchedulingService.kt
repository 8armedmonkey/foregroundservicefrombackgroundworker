package com.eightam.foregroundservice.timer

interface TimerNotificationSchedulingService {

    fun schedule(initialDelayMillis: Long)

}