package com.eightam.foregroundservice.timer

import androidx.lifecycle.LiveData

class RetrieveAllTimersLiveDataUseCase(private val timerRepository: TimerRepository) {

    fun execute(): LiveData<List<Timer>> {
        return timerRepository.getTimersLiveData()
    }

}