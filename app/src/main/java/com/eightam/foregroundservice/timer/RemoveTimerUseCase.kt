package com.eightam.foregroundservice.timer

import java.util.*

class RemoveTimerUseCase(private val timerRepository: TimerRepository) {

    fun execute(id: UUID) {
        timerRepository.remove(id)
    }

}