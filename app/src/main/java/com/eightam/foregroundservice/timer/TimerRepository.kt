package com.eightam.foregroundservice.timer

import androidx.lifecycle.LiveData
import java.util.*

interface TimerRepository {

    fun getTimersLiveData(): LiveData<List<Timer>>

    fun store(timer: Timer)

    fun retrieve(id: UUID): Timer

    fun remove(id: UUID)

    fun clear()

    fun size(): Int

}