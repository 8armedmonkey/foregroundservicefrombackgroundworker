package com.eightam.foregroundservice.utils.type

import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

class UUIDExtensionsTest {

    @Test
    fun whenConvertedToLongThenTheSameUUIDShouldProduceTheSameLongValue() {
        val uuid = UUID.randomUUID()

        val longValue1 = uuid.toLong()
        val longValue2 = uuid.toLong()

        assertEquals(longValue1, longValue2)
    }

    @Test
    fun whenConvertedToLongThenUUIDWithSameStringRepresentationShouldProduceTheSameLongValue() {
        val uuid1 = UUID.randomUUID()
        val uuid2 = UUID.fromString(uuid1.toString())

        assertEquals(uuid1.toLong(), uuid2.toLong())
    }

}