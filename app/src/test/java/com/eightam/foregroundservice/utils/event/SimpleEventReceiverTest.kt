package com.eightam.foregroundservice.utils.event

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.*

class SimpleEventReceiverTest {

    @Test
    fun `when no event has been received then it should indicate as having no new event`() {
        val receiver = SimpleEventReceiver.Builder<String>().build()

        assertFalse(receiver.hasNewEvents())
    }

    @Test
    fun `when event with new ID is received then it should indicate as having new event`() {
        val event = Event(id = UUID.randomUUID(), data = "Event")
        val receiver = SimpleEventReceiver.Builder<String>().build()

        receiver.receive(event)

        assertTrue(receiver.hasNewEvents())
        assertTrue(receiver.getNewEvents().contains(event))
    }

    @Test
    fun `when event is marked as old then it should be removed from the list of new events`() {
        val event = Event(id = UUID.randomUUID(), data = "Event")
        val receiver = SimpleEventReceiver.Builder<String>().build()

        receiver.receive(event)
        receiver.markEventsAsOld(eventIds = listOf(event.id))

        assertFalse(receiver.hasNewEvents())
        assertFalse(receiver.getNewEvents().contains(event))
        assertTrue(receiver.getOldEvents().contains(event))
    }

    @Test
    fun `when event with same ID as old event is received then it should not indicate as having new event`() {
        val event = Event(id = UUID.randomUUID(), data = "Event")
        val receiver = SimpleEventReceiver.Builder<String>().build()

        receiver.receive(event)
        receiver.markEventsAsOld(eventIds = listOf(event.id))
        receiver.receive(event)

        assertFalse(receiver.hasNewEvents())
        assertFalse(receiver.getNewEvents().contains(event))
    }

    @Test
    fun `when discarding new event then it should be removed from the list of new events`() {
        val event = Event(id = UUID.randomUUID(), data = "Event")
        val receiver = SimpleEventReceiver.Builder<String>().build()

        receiver.receive(event)
        receiver.discardEvents(eventIds = listOf(event.id))

        assertFalse(receiver.hasNewEvents())
        assertFalse(receiver.getNewEvents().contains(event))
    }

    @Test
    fun `when discarding old event then it should be removed from the list of old events`() {
        val event = Event(id = UUID.randomUUID(), data = "Event")
        val receiver = SimpleEventReceiver.Builder<String>().build()

        receiver.receive(event)
        receiver.markEventsAsOld(eventIds = listOf(event.id))
        receiver.discardEvents(eventIds = listOf(event.id))

        assertFalse(receiver.getOldEvents().contains(event))
    }

    @Test
    fun `when discarded event is received again then it should indicate as having new event`() {
        val event = Event(id = UUID.randomUUID(), data = "Event")
        val receiver = SimpleEventReceiver.Builder<String>().build()

        receiver.receive(event)
        receiver.discardEvents(eventIds = listOf(event.id))
        receiver.receive(event)

        assertTrue(receiver.hasNewEvents())
        assertTrue(receiver.getNewEvents().contains(event))
    }

    @Test
    fun `when built and oldEventIds contain UUID that is not in the events then it should be ignored`() {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val id3 = UUID.randomUUID()

        val event1 = Event(id = id1, data = "Event 1")
        val event2 = Event(id = id2, data = "Event 2")
        val event3 = Event(id = id3, data = "Event 3")

        val receiver = SimpleEventReceiver.Builder<String>()
            .events(listOf(event1, event2))
            .oldEventIds(setOf(id1, id2, id3))
            .build()

        receiver.receive(event3)

        assertTrue(receiver.hasNewEvents())
        assertTrue(receiver.getNewEvents().contains(event3))
    }

}